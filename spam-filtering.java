import java.util.*;

public class SpamFilter {
    public static void main(String[] args) {
        // Spam keywords
        final String[] SPAM_KEYWORDS = {
            "Glückwunsch",
            "Königin von Sabba",
            "viel Geld",
            "überweisen"
            };
        
        // Email examples
        List<String> mailBodys = new ArrayList<>();
        mailBodys.add(
            """
            Glückwunsch!

            Sie können nichts tun und dabei viel Geld verdienen.
            Machen Sie mit.
            Sie müssen nur 100,- € auf folgende IBAN überweisen:
            123456789

            Herzlichst
            Ihre Königin von Sabba
            """
            );
        mailBodys.add(
            """
            Herzlichen Glückwunsch zum Geburtstag, lieber Christoph!
            Sonnige Grüße aus Dresden sendet Dir Lavon.
            """
            );
        mailBodys.add(
            """
            Herzlichen Glückwunsch zum Geburtstag, liebe Bärbel!
            Ich freue mich auf unsere nächste Wanderung in den Alpen.
            """
            );
        mailBodys.add(
            """
            Herzlichen Glückwunsch, Arne.
            Die Königin von Sabba hat Ihnen 1000000,- DM geschenkt.
            Bitte überweisen Sie 100,- € an dieses Konto:
            123456789.
            """
        );
        mailBodys.add(
            """
            Herzlichen Glückwunsch, Nils.
            Die Königin von Sabba hat Ihnen 1000000,- DM geschenkt.
            Bitte überweisen Sie 100,- € an dieses Konto:
            123456789.
            """
        );
        mailBodys.add(
            """
            Glückwunsch.
            """
        );

        // Result
        for (String body : mailBodys) {
            boolean isSpam = isSpam(body, mailBodys, SPAM_KEYWORDS);
            System.out.println(body);
            System.out.println("---> ist " + (isSpam ? "Spam." : "kein Spam."));
            System.out.println();
            System.out.println();
        }
    }


    public static boolean isSpam(String body, List<String> mailBodys, String[] spamKeywords) {
        // Looking for spam keywords in emails
        int spamKeywordsInEmailBody = 0; 
        for (String keyword : spamKeywords) {
            if (body.toLowerCase().contains(keyword.toLowerCase())) {
                spamKeywordsInEmailBody++;
            }
        }
        // pseudo spam probability with logarithmus function
        double spamProbabilityByKeywords = Math.log(spamKeywordsInEmailBody + 1)/Math.log(spamKeywords.length + 1);

        // Checking similarity to other emails
        double maxSimilarity = 0;
        for (String otherBody : mailBodys) {
            if (!body.equals(otherBody)) {
                double similarity = calculateSimilarity(body, otherBody);
                if (similarity > maxSimilarity) {
                    maxSimilarity = similarity;
                }
            }
        }

        // average from both values: similarity to other emails and spam probability by blacklist
        double spamProbabilityFinal = (spamProbabilityByKeywords + maxSimilarity) / 2;

        return true ? spamProbabilityFinal > 0.5 : false;
    }


    public static double calculateSimilarity(String s1, String s2) {
        int distance = calculateLevenshteinDistance(s1, s2);
        int maxLength = Math.max(s1.length(), s2.length());
        return 1.0 - (double) distance / maxLength;
    }


    // https://www.baeldung.com/java-levenshtein-distance
    public static int calculateLevenshteinDistance(String x, String y) {
        int[][] dp = new int[x.length() + 1][y.length() + 1];

        for (int i = 0; i <= x.length(); i++) {
            for (int j = 0; j <= y.length(); j++) {
                if (i == 0) {
                    dp[i][j] = j;
                }
                else if (j == 0) {
                    dp[i][j] = i;
                }
                else {
                    dp[i][j] = min(dp[i - 1][j - 1]
                    + costOfSubstitution(x.charAt(i - 1), y.charAt(j - 1)), 
                    dp[i - 1][j] + 1, 
                    dp[i][j - 1] + 1);
                }
            }
        }

        return dp[x.length()][y.length()];
    }


    public static int costOfSubstitution(char a, char b) {
        return a == b ? 0 : 1;
    }


    public static int min(int... numbers) {
        return Arrays.stream(numbers)
          .min().orElse(Integer.MAX_VALUE);
    }
}